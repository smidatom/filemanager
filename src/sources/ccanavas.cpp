#include "../headers/ccanavas.h"

CCanavas::CCanavas()
{
	field = new CField(COL, LIN-2, 1);
	container = new CContainer*[NCONTAINERS];
	subroutine = new CSubRoutine(); 
	container[LEFT] = new CContainer(0, 1, 39, 23, field, subroutine);
	container[RIGHT] = new CContainer(0, 41, 39, 23, field, subroutine);
	activeContainer = LEFT;
}

CCanavas::~CCanavas()
{
	delete field;
	delete subroutine;
	delete container[LEFT];
	delete container[RIGHT];
	delete [] container;
}

/**
  * Clears the screen
  */

void CCanavas::Terminate()
{
	erase();
	endwin();
	return;
}

/**
  * Disables cursor ticking, turns on colormode and inits colors
  */

void CCanavas::Init()
{
	initscr();
	resizeterm(LIN, COL);
	cbreak();
	noecho();
	keypad(stdscr, TRUE);
	curs_set(0);
	start_color();
	init_pair(CYAN, COLOR_CYAN, COLOR_BLACK);
	init_pair(CYANR, COLOR_BLACK, COLOR_CYAN);
	init_pair(GREEN, COLOR_GREEN, COLOR_BLACK);
	init_pair(GREENR, COLOR_BLACK, COLOR_GREEN);
	init_pair(YELLOW, COLOR_YELLOW, COLOR_BLACK);
	init_pair(YELLOWR, COLOR_BLACK, COLOR_YELLOW);
	init_pair(WHITE, COLOR_WHITE, COLOR_BLACK);
	init_pair(WHITER, COLOR_BLACK, COLOR_WHITE);
	return;
}

/**
  * Prints a container in current status
  */

void CCanavas::PrintContainer(const int ID) const
{
	activeContainer==ID ? container[ID]->Print(true)
						: container[ID]->Print(false);
}

/**
  * Prints a simple string at the bottom of terminal to help user 
  * control the program 
  */

void CCanavas::PrintTail() const
{	
	attron(COLOR_PAIR(CYANR));
	attron(COLOR_PAIR(CYANR));
	for(int i=0; i<COL; i++)
		mvprintw(LIN-1, i, " ");
	mvprintw(LIN-1, 2, 
	"F4 MkSymlnk | F5 Copy | F6 Move | F7 Mkdir | F8 Delete | F9 Mkfile | Q quit");
	return;
}

/**
  * Switches active container
  */

void CCanavas::SwitchActiveContainer()
{
	activeContainer = activeContainer ? +LEFT : RIGHT;
	PrintContainer(+LEFT);
	PrintContainer(+RIGHT);
	return;
}

/**
  * Moves active selection by one file up
  */

void CCanavas::KeyUp()
{
	container[activeContainer]->SelectUp();
	return;
}

/**
  * Moves active selection by one file down
  */

void CCanavas::KeyDown()
{
	container[activeContainer]->SelectDown();
	return;
}

/**
  * Executes directory
  */

void CCanavas::Enter()
{
	container[activeContainer]->GetInto();
	return;
}

/**
  * Lets field call Draw() method
  */

void CCanavas::PrintField() const 
{
	field->Draw();
	return;
}

/**
  * Creates a new symbolic link in inactive container. New symbolic link
  * leads to selected file in active container 
  */

void CCanavas::KeyFFour()
{
	char name[256];
	field->MakeActive("Enter new symlink name: ");
	curs_set(1);
	echo();
	getstr(name);
	curs_set(0);
	noecho();
	string nameStr(name);
	if(field->Question(("Create new symlink: "
							 + nameStr + " ? (y/n) ")))
	{
		subroutine->MakeSymlink(container[activeContainer]->GetPath(),
		container[activeContainer]->GetSelected(),
		container[!activeContainer]->GetPath(),
		nameStr);
		container[activeContainer]->Refresh(true);
		container[!activeContainer]->Refresh(false);
	}
	field->Disable();
	return;
}

/**
  * Copys selected file to path of inactive container, after that
  * refreshes inactive container
  */

void CCanavas::KeyFFive()
{
	subroutine->Copy(container[activeContainer]->GetPath(),
		container[activeContainer]->GetSelected(),
		container[!activeContainer]->GetPath());
	container[!activeContainer]->Refresh(false);
}

/**
  * Moves a file from active container to inactive one, after that
  * refreshes both containers
  */

void CCanavas::KeyFSix()
{
	subroutine->Move(container[activeContainer]->GetPath(),
		container[activeContainer]->GetSelected(),
		container[!activeContainer]->GetPath());
	container[activeContainer]->Refresh(true);
	container[!activeContainer]->Refresh(false);
}

/**
  * Makes new directory in active container and refreshes both containers 
  */

void CCanavas::KeyFSeven()
{
	container[activeContainer]->MkDir();
	container[activeContainer]->Refresh(true);
	container[!activeContainer]->Refresh(false);
	return;
}

/**
  * Remove selected file and refreshses both containers
  */

void CCanavas::KeyFEight()
{
	container[activeContainer]->RmFile();
	container[activeContainer]->Refresh(true);
	container[!activeContainer]->Refresh(false);
	return;
}

/**
  * Create new file in active container and refreshes both containers
  */

void CCanavas::KeyFNine()
{
	container[activeContainer]->MkFile();
	container[activeContainer]->Refresh(true);
	container[!activeContainer]->Refresh(false);
	return;
}
