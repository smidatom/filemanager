#include "../headers/ccontainer.h"

CContainer::CContainer(const int & Lpos, const int & Cpos,
	 const int & width, const int & height, const CField * field, 
	 CSubRoutine * subroutine)
{
	this->Lpos = Lpos;
	this->Cpos = Cpos;
	this->width = width;
	this->height = height;
	path = "/";
	subroutine = subroutine;
	list = subroutine->List(path);
	selectedCFile = 0;
	headList = 0;
	maxItems = 18;
	this->field = field;
}

CContainer::~CContainer()
{
	while(!list->empty())
	{
        delete list->back();
     	list->pop_back();
    }
    delete list;
}

/**
  * Prints last 30 characters of path variable above the container
  * @param active Indicates if path should be printed in active/inactive
  * mode
  */

void CContainer::PrintPath(const bool active) const
{
	string maskPath;
	active ? attron(COLOR_PAIR(WHITE)) : attron(COLOR_PAIR(CYAN));
	if(path.size()>30)
	{	
		maskPath = path.substr(path.size()-31, 31);
		maskPath = "~" + maskPath;
		mvprintw(Lpos+1, Cpos+3, maskPath.c_str());
	}
	else
	{
		mvprintw(Lpos+1, Cpos+3, path.c_str());
	}	
}

/**
  * Prints horizontal borders of container
  * @param active Indicates if borders should be printed
  * in active/inactive mode
  */

void CContainer::PrintHorizontal(const bool active) const
{	
	active ? attron(COLOR_PAIR(WHITER)) : attron(COLOR_PAIR(CYANR));
	mvprintw(Lpos+2, Cpos, "      Name      Size     Modified     ");
	active ? attron(COLOR_PAIR(WHITE)) : attron(COLOR_PAIR(CYAN));
	for(int i=1; i<37; i++)
	{
		mvaddch(Lpos+1, Cpos+i, ACS_HLINE);
		mvaddch(Lpos+21, Cpos+i, ACS_HLINE);
	}
	return;
}

/**
  * Prints vertical borders of container
  * @param active Indicates if borders should be printed
  * in active/inactive mode
  */

void CContainer::PrintVertical(const bool active) const
{
	active ? attron(COLOR_PAIR(WHITE)) : attron(COLOR_PAIR(CYAN));
	for(int i=2; i<20; i++)
	{
		mvaddch(Lpos+i+1, Cpos, ACS_VLINE);
		mvaddch(Lpos+i+1, Cpos+37, ACS_VLINE);
	}
	mvaddch(Lpos+1, Cpos, ACS_ULCORNER);
	mvaddch(Lpos+1, Cpos+37, ACS_URCORNER);
	mvaddch(Lpos+21, Cpos, ACS_LLCORNER);
	mvaddch(Lpos+21, Cpos+37, ACS_LRCORNER);
	return;
}

/**
  * Prints list of files 
  * @param active Indicates if selected file should be printed
  * in active/inactive mode
  */

void CContainer::PrintList(const bool active) const
{
	int a=0;
	vector<CFile*>::iterator it = list->begin()+headList;
	while((a<maxItems) && it != list->end())
	{	
		active && a==(selectedCFile%maxItems) ?
			(*it)->Print(Lpos+a+3, Cpos+1, true) :
			(*it)->Print(Lpos+a+3, Cpos+1, false);
		it++;
		a++;
	}	
}

/**
  * Prints whole container
  * @param active Indicates if container should be printed
  * in active/inactive mode
  */

void CContainer::Print(const bool active) const
{	
	PrintHorizontal(active);
	PrintVertical(active);
	PrintPath(active);
	PrintList(active);
	return;
}

/**
  * Changes selection in list on file above
  */

void CContainer::SelectUp()
{
	vector<CFile*>::iterator it = list->begin()+selectedCFile;
	if(selectedCFile%maxItems)
	{
		(*it)->Print(Lpos+(selectedCFile%maxItems)+3, Cpos+1, false);
		it--;
		selectedCFile--;
		(*it)->Print(Lpos+(selectedCFile%maxItems)+3, Cpos+1, true);
		refresh();
	}
	else if(selectedCFile!=0 && selectedCFile%maxItems==0)
	{
		headList = headList - maxItems;
		for(int i=0; i<maxItems; i++)
			mvprintw(Lpos+i+3, Cpos+1, 
				"                                    ");
		selectedCFile--;
		PrintList(true);
	}
	return;
}

/**
  * Changes selection in list on file below
  */

void CContainer::SelectDown()
{
	vector<CFile*>::iterator it = list->begin()+selectedCFile;
	if(((selectedCFile%maxItems)+1)<maxItems && 
		selectedCFile<(signed)list->size()-1)
	{
		(*it)->Print(Lpos+(selectedCFile%maxItems)+3, Cpos+1, false);
		it++;
		selectedCFile++;
		(*it)->Print(Lpos+(selectedCFile%maxItems)+3, Cpos+1, true);
		refresh();
	}
	else if((selectedCFile%maxItems == maxItems-1) &&
			selectedCFile < (signed)list->size()-1)
	{
		headList = headList + maxItems;
		attron(COLOR_PAIR(CYAN));
		for(int i=0; i<maxItems; i++)
			mvprintw(Lpos+i+3, Cpos+1, 
				"                                    ");
		selectedCFile++;
		PrintList(true);
	}
	return;
}

/**
  * Changes path and prints content of selected directory
  */

void CContainer::GetInto()
{
	vector<CFile*>::iterator it = list->begin()+selectedCFile;
	if((*it)->UpdatePath(path))
	{
		CleanList();
    	list = subroutine->List(path);
    	PrintList(true);
    	PrintHorizontal(true);
    	PrintPath(true);
	}

}

/**
  * Creates a new directory at current path
  */

void CContainer::MkDir()
{
	char name[256];
	field->MakeActive("Enter a new directory name: ");
	curs_set(1);
	echo();
	getstr(name);
	curs_set(0);
	noecho();
	string nameStr(name);
	if(field->Question(("Create new directory: "
							 + nameStr + " ? (y/n) ")))
	{ 
		subroutine->MakeDir(path, nameStr);
	}
	field->Disable();
}

/**
  * Creates a new file at current path
  */

void CContainer::MkFile()
{
	char name[256];
	field->MakeActive("Enter new file name: ");
	curs_set(1);
	echo();
	getstr(name);
	curs_set(0);
	noecho();
	string nameStr(name);
	if(field->Question(("Create new file: "
							 + nameStr + " ? (y/n) ")))
	{
		subroutine->MakeFile(path, nameStr);
	}
	field->Disable();
}

/**
  * Deletes a selected file
  */

void CContainer::RmFile()
{
	if(field->Question("Delete " + path + GetSelected() + " ? (y/n) "))
	{
		subroutine->RemoveFile(path, GetSelected());
	}
	field->Disable();
	return;
}

/**
  * Removes all items from list variable
  */

void CContainer::CleanList()
{
	selectedCFile=0;
	headList = 0;
	while(!list->empty())
	{
        delete list->back();
     	list->pop_back();
   	}
    attron(COLOR_PAIR(CYAN));
   	for(int i=0; i<maxItems; i++)
		mvprintw(Lpos+i+3, Cpos+1, 
			"                                    ");
}

/**
  * @return Path variable
  */

const string & CContainer::GetPath()
{
	return path;
}

/**
  * @return Name of selected file 
  */

const string & CContainer::GetSelected()
{
	vector<CFile*>::iterator it = list->begin()+selectedCFile;
	return (*it)->GetName();
}

/**
  * Loads a list with files from path variable
  * @param active Indicates if parts of container should be printed
  * in active/inactive mode 
  */

void CContainer::Refresh(const bool active)
{
	CleanList();
   	list = subroutine->List(path);
   	PrintList(active);
   	PrintHorizontal(active);
    PrintPath(active);
}

