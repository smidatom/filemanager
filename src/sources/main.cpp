#include "../headers/ccanavas.h"
#include <iostream>

/**
  * @mainpage
  * <center>
  * <p>
  * A simple file manager, which allows user to make files, directories and
  * symbolic links. User is also allowed to move, copy, and remove files.<br>
  *	</p>
  * \image html snapshot.png
  * <h3>Controls: </h3>
  * <p>
  * Up and Down arrows to move in a list<br>
  * Tab to change active container<br>
  * F4 to create new symbolic link<br>
  * F5 to copy file<br>
  * F6 to move file<br>
  * F7 to create new directory<br>
  * F8 to remove file<br>
  * F9 to create file<br>
  * 'r' to reset<br>
  * <p>
  * <h3>Author:</h3>
  * Tomáš Šmida<br>
  * <br>
  * </center>
  */

int main()
{	
	int c;

	CCanavas * canavas = new CCanavas();
	canavas->Init();
	
	canavas->PrintContainer(+canavas->LEFT);
	canavas->PrintContainer(+canavas->RIGHT);
	canavas->PrintTail();
	canavas->PrintField();

	while((c=getch()) != 'q')
	{
		if(c==KEY_DOWN)
			canavas->KeyDown();
		else if(c==KEY_UP)
			canavas->KeyUp();
		else if(c=='\t')
			canavas->SwitchActiveContainer();
		else if(c=='\n')
			canavas->Enter();
		else if(c==KEY_F(4))
			canavas->KeyFFour();
		else if(c==KEY_F(5))
			canavas->KeyFFive();
		else if(c==KEY_F(6))
			canavas->KeyFSix();
		else if(c==KEY_F(7))
			canavas->KeyFSeven();
		else if(c==KEY_F(8))
			canavas->KeyFEight();
		else if(c==KEY_F(9))
			canavas->KeyFNine();
		else if(c==KEY_RESIZE || c=='r')
		{
			erase();
			canavas->PrintContainer(+canavas->LEFT);
			canavas->PrintContainer(+canavas->RIGHT);
			canavas->PrintTail();
			canavas->PrintField();
		}
	}

	canavas->Terminate();
	delete canavas;

	return 0;
}