#include "../headers/cfield.h"

CField::CField(const int size, const int Lpos, const int Cpos)
{
	this->size = size;
	this->Lpos = Lpos;
	this->Cpos = Cpos;
}

CField::~CField()
{
	;
}

/**
  * Draws a field
  */ 

void CField::Draw()
{
	attron(COLOR_PAIR(CYAN));
	mvprintw(Lpos, Cpos, ">> ");
}

/**
  * Prints content of field in active color
  * @param str A string which should be printed
  */

void CField::MakeActive(const string & str) const
{
	attron(COLOR_PAIR(WHITE));
	string toPrint = str;
	toPrint = ">> " + toPrint;
	mvprintw(Lpos, Cpos, toPrint.c_str());
}

/**
  * Returns field in disabled status
  */

void CField::Disable() const
{
	attron(COLOR_PAIR(CYAN));
	string toPrint = ">> ";
	for(int i=75; i>0; i--) toPrint +=" ";
	mvprintw(Lpos, Cpos, toPrint.c_str());
}

/**
  * Questions a user
  * @param question A string representing a question
  * @return true if answer is equal to 'y', otherwise false
  */

bool CField::Question(const string & question) const
{
	attron(COLOR_PAIR(WHITE));
	char answer[2];
	string toPrint = question;
	toPrint = ">> " + toPrint;
	if(toPrint.length()>70)
		toPrint = toPrint.replace(20, toPrint.length()-20-40, "~");
	mvprintw(Lpos, Cpos, toPrint.c_str());
	curs_set(1);
	echo();
	getstr(answer);
	curs_set(0);
	noecho();
	mvprintw(Lpos, 0, 
		"                                                              ");
	return answer[0]=='y';
}