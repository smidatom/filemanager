#include "../headers/cfile.h"

CFile::CFile(const string & name, const string & size,
			 const string & date)
{
	this->name = name;
	this->size = size;
	this->date = date;
}

/**
  * Prints a file at given coords
  * @param Cpos Number of column where string should be printed
  * @param Lpos Number of line where string should be printed
  * @param selected Indicates if file should be printed in active mode 
  */

void CFile::Print(const int Cpos, const int Lpos, bool selected) const
{
	string str;
	string nameMask;
	string sizeMask;
	MaskName(nameMask);
	MaskSize(sizeMask);
	str = nameMask +  " " + sizeMask + " " + date;
	selected ? attron(COLOR_PAIR(CYANR)) : attron(COLOR_PAIR(CYAN));
	mvprintw(Cpos, Lpos, str.c_str());
	return;
}

/**
  * Masks size string
  * @param str Size string
  */

void CFile::MaskSize(string & str) const
{
	string sizeMask;
	unsigned int temp;
	if(size.length()<5)
	{
		temp = size.length();
		while(temp<5)
		{
			sizeMask = sizeMask + " ";
			temp++;
		}
	}
	sizeMask = sizeMask + size;
	str = sizeMask;
	return;
}

/**
  * Masks file name
  * @param str Name string
  */

void CFile::MaskName(string & str) const
{
	int temp;
	if(name.length()>13)
	{
		str = name;
		str.replace(7, name.length()-10-2, "~");
	}
	else
	{
		temp = name.length();
		str = name;
		while(temp<13)
		{
			str = str + " ";
			temp++;
		}
	}
	return;
}

/**
  * @return always false, because ordinary file cannot be executed
  */ 

bool CFile::UpdatePath(string & name)
{
	return false;
}

/**
  * @return Name of file
  */

const string & CFile::GetName()
{
	return name;
}

CDir::CDir(const string & name, const string & size, const string & date)
			: CFile(name, size, date)
{
	;
}

/**
  * Prints a directory at given coords
  * @param Cpos Number of column where string should be printed
  * @param Lpos Number of line where string should be printed
  * @param selected Indicates if directory should be printed
  * in active mode 
  */

void CDir::Print(const int Cpos, const int Lpos, bool selected) const
{
	string str;
	string nameMask;
	string sizeMask;
	MaskName(nameMask);
	MaskSize(sizeMask);
	selected ? attron(COLOR_PAIR(GREENR)) : attron(COLOR_PAIR(GREEN));
	str = "/" + nameMask + " " + sizeMask + " " + date;
	mvprintw(Cpos, Lpos, str.c_str());
	return;
}

/**
  * Masks directory name
  * @param str Name string
  */

void CDir::MaskName(string & str) const
{
	unsigned int temp;
	if(name.length()>12)
	{
		str = name;
		str.replace(6, name.length()-9-2, "~");
	}
	else
	{
		temp = name.length();
		str = name;
		while(temp<12)
		{
			str = str + " ";
			temp++;
		}
	}
	return;
}

/**
  * Adds directory name to a given path string
  * @param path A string where is directory name appended
  * @return false if directory name is "..", otherwise true
  */

bool CDir::UpdatePath(string & path)
{
	size_t lastDash;
	if(name=="..")
	{
		if(path=="/")
			return false;
		else
		{
			path = path.erase(path.length()-1);
			lastDash = path.find_last_of("/");
			path = path.erase(lastDash+1);
			return true;
		}
	}
	else
	{
		path = path + name + "/";
		return true;
	}
}

CSymlink::CSymlink(const string & name, const string & size,
					const string & date, const bool executable)
					: CFile(name, size, date)
{
	this->executable = executable;
}

/**
  * Prints a file at given coords
  * @param Cpos Number of column where string should be printed
  * @param Lpos Number of line where string should be printed
  * @param selected Indicates if file should be printed in active mode 
  */

void CSymlink::Print(const int Cpos, const int Lpos, bool selected) const
{
	string str;
	string nameMask;
	string sizeMask;
	MaskName(nameMask);
	MaskSize(sizeMask);
	selected ? attron(COLOR_PAIR(YELLOWR)) : attron(COLOR_PAIR(YELLOW));
	executable ? str = "/" + nameMask + " " + sizeMask + " " + date :
				 str = nameMask + " " + sizeMask + " " + date;
	mvprintw(Cpos, Lpos, str.c_str());
	return;
}

/**
  * Masks symbolic link name
  * @param str Name string
  */

void CSymlink::MaskName(string & str) const
{
	return (executable ? CDir(name, size, date).MaskName(str)
					   : CFile(name, size, date).MaskName(str));
}

/**
  * Adds symbolic name to a given path string
  * @param path A string where is symbolic link name appended
  * @return true if symbolic link points at directory, otherwise false
  */

bool CSymlink::UpdatePath(string & path)
{
	return (executable ? CDir(name, size, path).UpdatePath(path)
					   : CFile(name, size, path).UpdatePath(path));	
}