#include "../headers/csubroutine.h"

CSubRoutine::CSubRoutine()
{
	;
}

/**
  * Method calls program ls, modify output through other programs
  * and then fills a list with CFile objects
  * @param path An argument for ls program
  * @return A vector with files at path given as an argument
  */

vector<CFile*> * CSubRoutine::List(const string & path)
{
	vector<CFile*> * list = new vector<CFile*>();
	string script, type, name, size, date, hours, temp;
	CFile * file;
	CDir * dir;
	CSymlink * symlink;
	stringstream ss;
	char buff[1024];
	script = "ls -lh --time-style long-iso '" + path 
 		+ "' 2> /dev/null | tail -n +2 | sort -k 1.1,1.2 -k 8.1 -f | tr -s \" \" | cut -d\" \" -f1,5,6,7,8- ";

	FILE * stream;

	if(!(stream = popen(script.c_str(), "r")))
	{
		dir = new CDir("..", "    ", "                ");
		list->push_back(dir);
		return list;
	}

	dir = new CDir("..", "    ", "                ");
	list->push_back(dir);
	size_t spaceEnd;
	string line;
	while(fgets(buff, sizeof(buff), stream)!=NULL)
	{
		ss << buff;
		type = ss.str().substr(0, 1);
		line = ss.str().substr(0, ss.str().length());
		line = line.substr(11, line.length()-11);
		spaceEnd = line.find_first_of(" ");
		size = line.substr(0, spaceEnd);
		date = line.substr(spaceEnd+1, 16);
		name = line.substr(spaceEnd+1+16+1, line.length()-spaceEnd-19);
		if(type[0]=='d')
		{
			dir = new CDir(name, size, date);
			list->push_back(dir);
		}
		else if(type[0]=='l')
		{	
			size_t arrow;
			char out[512];
			bool executable;
			arrow = name.find(">");
			name = name.substr(0, arrow-2);
			FILE * test = 
			popen(("test -d 2> /dev/null \"`readlink 2> /dev/null '"
			 + path + name + "' `\" && echo 1 || echo 0").c_str(), "r");
			fgets(out, sizeof(out), test);
			executable = out[0]=='0' ? false : true;
			symlink = new CSymlink(name, size, date, executable);
			list->push_back(symlink);
			pclose(test);
		}
		else
		{	
			file = new CFile(name, size, date);
			list->push_back(file);
		}
		name.clear(); size.clear(); date.clear();
		ss.str("");
	}
	pclose(stream);

	return list;
}

/**
  * Makes new directory through mkdir
  * @param path A place where should be new directory created
  * @param name A name of new directory
  */

void CSubRoutine::MakeDir(const string & path, const string & name)
{
	string script = "mkdir 2> /dev/null '" + path + name + "'";
	FILE * p = popen(script.c_str(), "r");
	pclose(p);
	return;
}

/**
  * Copies a selected file to a destPath
  * @param sourcePath A path where is file to copy
  * @param sourceName A name of file to copy
  * @param destPath A path where should be file copied
  */

void CSubRoutine::Copy(const string & sourcePath, 
			const string & sourceName, const string & destPath)
{
	if(sourcePath!=destPath)
	{
		string script = "cp -r 2> /dev/null '" + sourcePath + sourceName 
		+ "' '" + destPath + "'";
		FILE * p = popen(script.c_str(), "r");
		pclose(p);
	}
	return;
}

/**
  * Removes file from given path. If the path is equal to "/" or 
  * filename is equal to ".." method does nothing
  * @param path A path where is file to remove
  * @param fileName A name of file to remove
  */

void CSubRoutine::RemoveFile(const string & path, const string & fileName)
{	
	if(path!="/" && fileName!="..")
	{
		string script = "rm -r 2> /dev/null '" + path + fileName + "'";
		FILE * p = popen(script.c_str(), "r");
		pclose(p);
	}
	return;
}

/**
  * Makes a symbolic link through ln -s program
  * @param path A path where is a file where symlink should link
  * @param sourceName A name of a file where symlink should link
  * @param destPath A path where should be new symlink created
  * @param symlinkName a name of new symbolic link
  */

void CSubRoutine::MakeSymlink(const string & path, 
	const string & sourceName, const string & destPath, 
								const string & symlinkName)
{
	string script = "ln -s 2> /dev/null '" + path + sourceName + "' '" +
	destPath + symlinkName + "'";
	FILE * p = popen(script.c_str(), "r");
	pclose(p);
	return;
}

/**
  * Moves file form one path to another through mv program
  * @param sourcePath A path where is file to move
  * @param sourceName A name of a file to move
  * @param destPath A path where should be file moved
  */

void CSubRoutine::Move(const string & sourcePath, 
			const string & sourceName, const string & destPath)
{
	if(sourcePath!=destPath)
	{
		string script = "mv 2> /dev/null '" + sourcePath + sourceName + "' '"
					 + destPath + "'";
		FILE * p = popen(script.c_str(), "r");
		pclose(p);
	}
	return;
}

/**
  * Makes new file through touch program
  * @param path A path where shoul be new file created
  * @param fileName A name of new file
  */

void CSubRoutine::MakeFile(const string & path, const string & fileName)
{
	string script = "touch 2> /dev/null '" + path + fileName + "'";
	FILE * p = popen(script.c_str(), "r");
	pclose(p);
	return;
}
