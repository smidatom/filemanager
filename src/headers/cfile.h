#ifndef __CFILE_H_INCLUDED__
#define __CFILE_H_INCLUDED__

using namespace std;
#include <string>
#include <ncurses.h>

/**
  * A simple enum to mask color ID. ID has to be greater than zero.
  */ 

typedef enum
{
	CYAN=1, CYANR, GREEN, GREENR, YELLOW, YELLOWR, WHITE, WHITER
} Colors;

/**
  * @brief A class to represent ordinary non-executable file.
  */

class CFile
{
	public:
		CFile(const string & name, const string & size, const string & date);
		virtual ~CFile() {};
		virtual void Print(const int Lpos, const int Cpos,
							bool selected) const;
		virtual void MaskName(string & str) const;
		void MaskSize(string & str) const;
		virtual bool UpdatePath(string & path);
		virtual const string & GetName();
	protected:
		string name, size, date;
};

/**
  * @brief A class derived from CFile which represents a directory.
  *
  *	A CDir is only executable file in this program.
  */

class CDir : public CFile
{
	public:
		CDir(const string & name, const string & size, const string & date);
		virtual void MaskName(string & str) const;
		virtual void Print(const int Lpos, const int Cpos,
							bool selected) const;
		virtual bool UpdatePath(string & path);
	protected:
};

/**
  * @brief A class derived from CFile which represents a symbolic link.
  *
  * Its behavior depends on the file which is symbolic link linked to
  */

class CSymlink : public CFile
{
	public:
		CSymlink(const string & name, const string & size,
					const string & date, const bool executable);
		virtual void MaskName(string & str) const;
		virtual void Print(const int Lpos, const int Cpos,
							 bool selected) const;
		virtual bool UpdatePath(string & path);
	protected:
		bool executable;
};

#endif //__CFILE_H_INCLUDED__