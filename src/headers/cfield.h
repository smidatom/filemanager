#ifndef __CFIELD_H_INCLUDED__
#define __CFIELD_H_INCLUDED__

#include "cfile.h"

/**
  * @brief A class to represent a simple one-line form for interaction
  * between program and user
  */

class CField
{
	public:
		CField(const int size, const int Lpos, const int Cpos);
		~CField();
		void Draw();
		void MakeActive(const string & str) const;
		void Disable() const;
		bool Question(const string & question) const;
	protected:
		int size, Lpos, Cpos;
};

#endif // __CFIELD_H_INCLUDED__
