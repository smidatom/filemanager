#ifndef __CCONTAINER_H_INCLUDED__
#define __CCONTAINER_H_INCLUDED__

#include "csubroutine.h"
#include "cfield.h"

/**
  * @brief A class to represent a panel which shows files at current path.
  */

class CContainer
{
	public:
		CContainer(const int & Lpos, const int & Cpos, const int & width,
					const int & height, const CField * field,
					CSubRoutine * subroutine);
		~CContainer();
		void Print(const bool active) const;
		void SelectUp();
		void SelectDown();
		void MkDir();
		void MkFile();
		void RmFile();
		void GetInto();
		void Refresh(const bool active);
		const string & GetPath();
		const string & GetSelected();
	protected:
		void PrintPath(const bool active) const;
		void PrintList(const bool active) const;
		void PrintHorizontal(const bool active) const;
		void PrintVertical(const bool active) const;
		void CleanList();
		int Lpos, Cpos, width, height, selectedCFile,
						headList, maxItems;
		string path;
		vector<CFile*> * list;
		const CField * field;
		CSubRoutine * subroutine;
};

#endif //__CCONTAINER_H_INCLUDED__