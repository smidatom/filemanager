#ifndef __CSUBROUTINE_H_INCLUDED__
#define __CSUBROUTINE_H_INCLUDED__

#include <cstdio>
#include <sstream>
#include <vector>

#include "cfile.h"

/**
  * @brief A class which covers all actions with selected file.
  * 
  * All actions are done by creating new pipeline so user doesn't 
  * have to wait until the action is done. 
  */

class CSubRoutine
{
	public:
		CSubRoutine();
		~CSubRoutine() {};
		vector<CFile*> * List(const string & path);
		void MakeDir(const string & path, const string & dirName);
		void MakeFile(const string & path, const string & fileName);
		void MakeSymlink(const string & path, const string & sourceName,
			const string & destPath, const string & symlinkName);
		void Copy(const string & sourcePath, const string & sourceName,
					 const string & destPath);
		void Move(const string & sourcePath, const string & sourceName,
					 const string & destPath);
		void RemoveFile(const string & path, const string & fileName);
	protected:
};

#endif // __CSUBROUTINE_H_INCLUDED__