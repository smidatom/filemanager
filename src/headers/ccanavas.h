#ifndef __CCANAVAS_H_INCLUDED__
#define __CCANAVAS_H_INCLUDED__

#include "ccontainer.h"
#include "cfield.h"

/**
  * @brief A class to represent ncurses enviroment.
  */
/**
  * Covers basic I/O opeartions, between user and active container. 
  */

class CCanavas
{
	public:
		CCanavas();
		~CCanavas();
		void Init();
		void Terminate();
		void PrintContainer(const int ID) const; 
		void PrintTail() const;
		void PrintField() const;
		void KeyFFour();
		void KeyFFive();
		void KeyFSeven();
		void KeyFSix();
		void KeyFEight();
		void KeyFNine();
		void KeyUp();
		void KeyDown();
		void Enter();
		void SwitchActiveContainer();
		static const int LEFT = 0;
		static const int RIGHT = 1;
	protected:
		CContainer ** container;
		CField * field;
		CSubRoutine * subroutine;
		int activeContainer;
		static const int NCONTAINERS = 2;
		static const int COL = 80;
		static const int LIN = 24;
};

#endif //__CCANAVAS_H_INCLUDED__