#Makfile je silne inspirovan makefilem pana Ing. Jana Baiera,
#vystavenem na eduxu.
#Panu Baierovy timto velice dekuji za usetrenou namahu.

CXX=g++
CXXFLAGS=-Wall -pedantic -Wno-long-long -O0 -ggdb -g
LDFLAGS=-lncurses
EXECUTABLE=smidatom
USERNAME=smidatom 
SOURCES=./src/sources/main.cpp ./src/sources/ccontainer.cpp ./src/sources/ccanavas.cpp ./src/sources/cfile.cpp ./src/sources/csubroutine.cpp ./src/sources/cfield.cpp
RM=rm -r
DOC=doc

all: compile doc

compile: $(SOURCES:.cpp=.o)
	$(CXX) $(SOURCES:.cpp=.o) $(LDFLAGS) -o $(EXECUTABLE)

run:
	./$(EXECUTABLE)

clean:
	$(RM) $(SOURCES:.cpp=.o) $(EXECUTABLE) ./$(DOC)

doc:
	doxygen ./src/doxygen/Doxyfile